import customLoading from './modules/custom-loading'
import authenticated from './modules/authenticated'
import menu from './modules/menu'
import testData from './modules/test-data'
import member from './modules/member'
import board from './modules/board'
import goods from './modules/goods'
import comment from './modules/comment'

export const state = () => ({})

export const mutations = {}

export const modules = {
    customLoading,
    authenticated,
    menu,
    testData,
    member,
    board,
    goods,
    comment,

}
