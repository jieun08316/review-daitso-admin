const state = () => ({
    globalMenu: [
        {
            parentName: 'HOME',
            menuLabel: [
                { id: 'DASH_BOARD', currentName: '대시보드', link: '/', isShow: true },
            ]
        },


        {
            parentName: '회원관리',
            menuLabel: [
                { id: 'MEMBER_LIST', currentName: '회원조회(수정)', link: '/mem-list', isShow: true },
                { id: 'MEMBER_LEVEL', currentName: '레벨관리', link: '/mem-level', isShow: false },
                { id: 'MEMBER_STATUS', currentName: '상태관리 ', link: '/mem-status', isShow: false },
                { id: 'MEMBER_DELETE', currentName: '회원추방/탈퇴', link: '/mem-delete', isShow: false },
            ]
        },        {
            parentName: '컨텐츠관리',
            menuLabel: [
                { id: 'CONTENTS_BOARD', currentName: '게시판관리', link: '/con-board', isShow: true },
                { id: 'CONTENTS_POST', currentName: '게시글관리', link: '/con-post', isShow: true },
                { id: 'CONTENTS_REVIEW', currentName: '댓글관리', link: '/con-review', isShow: true },
                { id: 'CONTENTS_QR', currentName: 'QR 관리', link: '/con-qr', isShow: true },
                { id: 'CONTENTS_FORBIDDEN_WORD', currentName: '금칙어 관리', link: '/con-forbidden-word', isShow: true },
            ]
        },        {
            parentName: '운영관리',
            menuLabel: [
                { id: 'COMPANY_AD', currentName: '광고현황', link: '/com-ad', isShow: true },
                { id: 'COMPANY_AD_NEW', currentName: '광고신규문의', link: '/com-ad-new', isShow: true },
                { id: 'COMPANY_STATISTICS', currentName: '통계현황', link: '/com-statistics', isShow: true },

            ]
        },
    ],
    selectedMenu: 'DASH_BOARD'
})

export default state
