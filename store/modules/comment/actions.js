import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {

    [Constants.DO_LIST_PAGING]: (store, payload) => {
        return axios.get(apiUrls.DO_LIST_PAGING.replace('{pageNum}', payload.pageNum))
    },
}
