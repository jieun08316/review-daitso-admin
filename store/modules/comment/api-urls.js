const BASE_URL = '/v1/comment'

export default {
    DO_LIST_PAGING: `${BASE_URL}/all/{pageNum}`, //get
}
