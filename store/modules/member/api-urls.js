const BASE_URL = '/v1/member'

export default {
    DO_MEMBER_LiST_GET: `${BASE_URL}/all`, //get List

    DO_LIST_PAGING: `${BASE_URL}/all/{pageNum}`, //get Page
}
