import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_MEMBER_LiST_GET]: (store) => {
        console.log(store)
        return axios.get(apiUrls.DO_MEMBER_LiST_GET)
    },

    [Constants.DO_LIST_PAGING]: (store, payload) => {
        return axios.get(apiUrls.DO_LIST_PAGING.replace('{pageNum}', payload.pageNum))
    },
}
