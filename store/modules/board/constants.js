export default {
    DO_BOARD_LIST: 'board/doBoardList',

    DO_LIST_PAGING: 'board/doListPaging'
}
