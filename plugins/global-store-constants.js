import Vue from 'vue'

import customLoadingConstants from '~/store/modules/custom-loading/constants'
Vue.prototype.$customLoadingConstants = customLoadingConstants

import authenticatedConstants from '~/store/modules/authenticated/constants'
Vue.prototype.$authenticatedConstants = authenticatedConstants

import menuConstants from '~/store/modules/menu/constants'
Vue.prototype.$menuConstants = menuConstants

import testDataConstants from '~/store/modules/test-data/constants'
Vue.prototype.$testDataConstants = testDataConstants

import memberConstants from '~/store/modules/member/constants'
Vue.prototype.$memberConstants = memberConstants

import boardConstants from '~/store/modules/board/constants'
Vue.prototype.$boardConstants = boardConstants

import goodsConstants from '~/store/modules/goods/constants'
Vue.prototype.$goodsConstants = goodsConstants

import commentConstants from '~/store/modules/comment/constants'
Vue.prototype.$commentConstants = commentConstants


